module.exports = {
    chainWebpack: (config) => {
        // Clear the existing entry point
        config.entry('app').clear();

        // Add the custom entry point
        config.entry('app').add('./src/main.ts');
    }
};
