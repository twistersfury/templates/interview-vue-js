######################################################
# Core Configurations (Do Not Edit)
######################################################
BIN_PATH ?= bin

MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

######################################################
# Initial Values (Overrides Included Defaults)
######################################################

DOCKER_IMAGE := twistersfury/vuejs-sample:latest
DOCKER_PORTS := --publish 8090:80/tcp
DOCKER_VOLUMES ?= --volume "$(CURDIR)"://app

######################################################
# Default Command (Must Be First)
######################################################
.PHONY: default
default: test

######################################################
# Include Helm
######################################################
$(BIN_PATH)/helm.mk: Makefile | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.3.0/make/helm.mk

ifndef MAKEFILE_HELM
include $(BIN_PATH)/helm.mk
endif


######################################################
# Include Node
######################################################
$(BIN_PATH)/node.mk: Makefile | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.3.1/make/node.mk

ifndef MAKEFILE_NODE
include $(BIN_PATH)/node.mk
endif

######################################################
# Include Alpha Docker
######################################################
$(BIN_PATH)/docker.mk: Makefile | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v3.0.0-alpha+18/make/docker.mk

ifndef MAKEFILE_DOCKER
include $(BIN_PATH)/docker.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	@echo 'MAKEFILE_OVERRIDES := true' > $@
	@echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	@echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	@echo "Override File Created, You Will Have To Rerun The Previous Command."
	@echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	@echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
include $(BIN_PATH)/overrides.mk
endif

######################################################
# Core Commands
######################################################

.PHONY: clean
clean:
	rm -rf vendor
	rm -rf bin

