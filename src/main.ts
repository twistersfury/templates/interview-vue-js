import { createApp } from 'vue';
// @ts-ignore
import App from '@/App.vue';

import '@/scss/main.scss';

createApp(App).mount('#app');
